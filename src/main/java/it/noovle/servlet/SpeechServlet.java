package it.noovle.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.repackaged.com.google.gson.Gson;
import com.google.cloud.speech.grpc.demos.NonStreamingRecognizeClient;
import com.google.cloud.speech.v1.NonStreamingRecognizeResponse;

/**
 * Servlet implementation class SpeechServlet
 */
public class SpeechServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static String host = "speech.googleapis.com";
	private static Integer port = 443;
	private static String tmpAudioFile = "/tmp/tmp.flac";


    /**
     * Default constructor. 
     */
    public SpeechServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Gson gson = new Gson();
		
		String lang = request.getHeader("lang");
		Integer sampling = Integer.parseInt(request.getHeader("sampling"));
		InputStream body = request.getInputStream();
	    
		File f = new File(tmpAudioFile);
		
		if(f.exists()) {
			f.delete();
		}
		
		// write the inputStream to a FileOutputStream
		OutputStream outputStream = null;
		
		outputStream = new FileOutputStream(f);

		int read = 0;
		byte[] bytes = new byte[1024];

		while ((read = body.read(bytes)) != -1) {
			outputStream.write(bytes, 0, read);
		}
		
		outputStream.close();
		
		String creadFileIs = "/usr/local/speech/noovle-cloud-speech-demo-ad924a3b65b9.json";
		InputStream in = new FileInputStream(creadFileIs);

		NonStreamingRecognizeClient client = new NonStreamingRecognizeClient(host, port, tmpAudioFile, sampling, lang, in);
		NonStreamingRecognizeResponse res = null;
		try {
			res = client.recognize();
		} finally {
			try {
				client.shutdown();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		String jsonRes = gson.toJson(res.getAllFields());
		
		response.getWriter().append(jsonRes); 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
