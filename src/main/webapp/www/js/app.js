'use strict';

angular.module('SpeechCtrl', [ //
'ngRoute',//
'ngMaterial', //
'pascalprecht.translate',//
'UploadModule', //
'SpeechSvcModule', //
'uploadFileDirective' //
]);

angular.module('SpeechCtrl').config(
		[ '$translateProvider', function($translateProvider) {

			$translateProvider.useStaticFilesLoader({
				prefix : 'i18n/',
				suffix : '.json'
			});

			$translateProvider.preferredLanguage('en_US');
		} ]);

angular.module('SpeechCtrl').config(
		[ '$routeProvider', function($routeProvider) {
			$routeProvider.when('/upload', {
				templateUrl : 'partials/upload.html',
			}).otherwise({
				redirectTo : '/upload'
			});
		} ]);
