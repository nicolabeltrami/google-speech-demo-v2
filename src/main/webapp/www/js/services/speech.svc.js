'use strict';

angular.module('SpeechSvcModule', []);

(function() {
	angular.module('SpeechSvcModule').constant('RESTURL_SPEECH', {
		callSpeechApi : '/noovle-cloud-speech-demo-V2/SpeechServlet',
	});
})();

(function() {

	angular.module('SpeechSvcModule').service('SpeechSvc', [ //
	'$http', //
	'$log', //
	'$q', //
	'RESTURL_SPEECH', //
	SpeechSvc ]);

	function SpeechSvc($http, $log, $q, RESTURL_SPEECH) {

		this._callSpeechApi = function _callSpeechApi(lang, sample, files) {
			
			var fd = new FormData();
			for ( var i = 0; i < files.length; i++)
			{
				  fd.append("file", files[i]);
			}

			return $http.post(RESTURL_SPEECH.callSpeechApi, fd, {
				headers : {
					'sampling' : sample,
					'lang' : lang,
					'Content-Type': 'undefined'
				},
				params : {}
			}).then(function(res) {
				return res;
			}, function(err) {
				$log.error("Error to parse speech audio file");
			});

		};

	}

})();
