'use strict';

angular.module('UploadModule', []).controller(
		'UploadCtrl',
		[//
				'$scope', //
				'$log', //
				'SpeechSvc', //
				function($scope, //
				$log, //
				SpeechSvc) {
					$scope.audio = {};
					// Default audio values
					$scope.audio.lang = "it-IT";
					$scope.audio.samplerate = 16000;
					$scope.result = null;
					$scope.isLoading = false;

					$scope.calculate = function calculate() {

						$log.info("Chiamo il servizio");
						
						$scope.isLoading = true;

						SpeechSvc._callSpeechApi($scope.audio.lang,
								$scope.audio.samplerate, $scope.files).then(
								function(res) {
									$scope.isLoading = false;
									$scope.result = JSON.stringify(res, undefined, 2);
								});

					}

					$scope.uploadedFile = function(element) {
						$scope.$apply(function($scope) {
							$scope.files = element.files;
							console.log($scope.files);
						});
					}

				} ]);
