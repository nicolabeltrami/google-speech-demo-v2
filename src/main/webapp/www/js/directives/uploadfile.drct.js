'use strict';

angular.module('uploadFileDirective', []);
angular.module('uploadFileDirective').directive('onReadFile', ['$parse', function($parse) {
	return {
		restrict: 'A',
		scope: false,
		link: function(scope, element, attrs) {
			var fn = $parse(attrs.onReadFile);
            
			element.on('change', function(onChangeEvent) {
				
				var reader = new FileReader();
                
				reader.onload = function(onLoadEvent) {
					scope.$apply(function() {
						console.log("Arrivo qui");
						fn(scope, {$fileContent:onLoadEvent.target.result});
					});
				};

				reader.readAsText((onChangeEvent.srcElement || onChangeEvent.target).files[0]);
			});
		}
	};
}]);